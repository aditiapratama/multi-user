============
Installation
============

.. hint::
    The process is the same for linux, mac and windows.

1. Download latest `release <https://gitlab.com/slumber/multi-user/-/jobs/artifacts/master/download?job=build>`_ or `develop (unstable !) <https://gitlab.com/slumber/multi-user/-/jobs/artifacts/develop/download?job=build>`_ build.
2. Run blender as administrator (to allow python dependencies auto-installation).
3. Install **multi-user.zip** from your addon preferences.

Once the addon is succesfully installed, I strongly recommend you to follow the :ref:`quickstart`
tutorial.